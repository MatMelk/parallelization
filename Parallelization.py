#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 15:11:17 2020

@author: m16035433
"""

from multiprocessing import Pool

f=open('seqfasta.fa','r')  
s=f.read()
f.close()

l=[]
chev=0
while chev!=-1:
    supr=s.find('\n')
    s=s.replace(s[0:supr],'')
    chev=s.find('>')
    l.append(s[0:chev].replace('\n',''))
    s=s.replace(s[0:chev],'')



def NbATCG(s):
    nb={'A':s.count("A"),
    'T':s.count("T"),
    'C':s.count("C"),
    'G':s.count("G")}
    return nb


if __name__ == '__main__':
    p = Pool(len(l))
    print(p.map(NbATCG,l))
